FROM registry.access.redhat.com/rhel7.3
MAINTAINER toni@stderr.at

ENV PROM_VERSION 1.4.1
ENV PROM_FILE    prometheus-${PROM_VERSION}.linux-amd64
ENV PROM_TAR     ${PROM_FILE}.tar.gz

COPY ${PROM_TAR} /tmp

RUN mkdir -p /opt/prometheus \
 && mkdir -p /etc/prometheus \
 && mkdir -p /prometheus \
 && cd /opt \
 && tar zxf /tmp/${PROM_TAR} -C /opt/prometheus --strip-components=1

COPY prometheus.yml /etc/prometheus/prometheus.yml

EXPOSE     9090 
VOLUME     [ "/prometheus" ]
WORKDIR    /prometheus
ENTRYPOINT [ "/opt/prometheus/prometheus" ]
CMD        [ "-config.file=/etc/prometheus/prometheus.yml", \
             "-storage.local.path=/prometheus", \
             "-web.console.libraries=/opt/prometheus/console_libraries", \
             "-web.console.templates=/opt/prometheus/consoles" ]
